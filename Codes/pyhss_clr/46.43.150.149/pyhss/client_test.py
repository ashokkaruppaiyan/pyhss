import socket
import sctp
import diameter
import _thread

diameter_host = 'vectone.sp.thhss4.epc.mnc012.mcc214.3gppnetwork.org'                                                       #Diameter Host of this Machine
hostname = "62.87.67.160" 
realm = "epc.mnc012.mcc214.3gppnetwork.org"  
mcc = '214'                                                                     #Mobile Country Code
mnc = '01'
DestinationRealm = "xisxhblsqpupqurr1255wg4xhxirs1yvqk3lizmq.epc.mnc001.mcc214.3gppnetwork.org"                                             #Diameter Host of Destination
DestinationHost = "epc.mnc001.mcc214.3gppnetwork.org"                                                #Diameter Realm of Destination

diameter = diameter.Diameter(diameter_host, realm, 'PyHSS-client', str(mcc), str(mnc))

clientsocket = sctp.sctpsocket_tcp(socket.AF_INET)
clientsocket.connect((hostname,3868))

def ReadBuffer():
    data = clientsocket.recv(32)
    print("Data is ", data.decode())

def SendRequest(request):
    #print (request)
    clientsocket.sendall(bytes.fromhex(request)) 
    #print ("request000000")

_thread.start_new_thread(ReadBuffer,())       
while True:
#     imsi = "214120000000049"
#     print("Sending Cancel Location Request to " + str(hostname))
#     SendRequest(diameter.Request_16777251_317(imsi, DestinationHost, DestinationRealm))        
    ReadBuffer()
  