README for PyHSS:-

Project : PyHSS
Vendor : Nick Jones
Email : Nick Jones <nick@nickvsnetworking.com>
Year : 2021

Details:

URL : https://github.com/nickvsnetworking/pyhss
Branch : master

Document URL : https://docs.google.com/document/d/1fv1Bak7x0OoAHCSESR9ixj-ZVBukrIspPTfNVVyb3k0/edit

Monitoring : https://github.com/nickvsnetworking/pyhss/blob/master/docs/monit.md

Vectone Repo : https://bitbucket.org/ashokkaruppaiyan/pyhss/src/master/

---
Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).
